#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "hardware.h"
#include "simple_uart.h"
#include "bit_operations.h"

static inline void init_timer() {
    TCCR1A = (
        (1<<WGM10) |
        (1<<WGM11) |
        (1<<COM1B1)
    );

    TCCR1B = (
        (1<<ICES1) |    // rising edge capture
        (1<<CS10)  |    // F_CPU / 1 prescaler
        (1<<WGM13) |
        (1<<WGM12)
    );
    
    OCR1A = 0xFFFF;
    OCR1B = 0x7FFF;

    TIMSK |= (1<<TOIE1) | (1<<ICIE1);
}

static inline void init() {
    DDRB = DDRB_STATE;
    DDRD = DDRD_STATE;

    PORTB = PORTB_STATE;
    PORTD = PORTD_STATE;

    _delay_ms(500);

    init_timer();
    uart_init();
    
    sei();
}

static inline void soft_reset()
{
    wdt_enable(WDTO_30MS);
    for(;;);
}

uint16_t overflows;
uint16_t prev_overflows;
uint16_t clocks;
uint16_t prev_clocks;


uint32_t period;
uint8_t timer;
uint8_t prev_timer;

int8_t initial_difference_sign = 0;
uint8_t initial_countdown = 2;

static inline void modify_pwm(int32_t difference) {
    int8_t sign = (difference > 0) - (difference < 0);
    if (initial_difference_sign == 2) {
        initial_difference_sign = sign;
    }

    if (initial_countdown > 1) {
        if (initial_countdown == 2) {
            initial_difference_sign = sign;
        }
        initial_countdown--;
        return;
    }

    if (initial_countdown == 1) {
        if (initial_difference_sign != sign) {
            initial_countdown = 0;
        }
    }

    if (initial_countdown == 1) {
        difference = 1300 * sign;
    } else {
        difference = sign;
    }

    int32_t new_value = OCR1B + difference;
    if (new_value > 0xFFFF) {
        OCR1B = 0xFFFF;
    } else if (new_value < 0) {
        OCR1B = 0;
    } else {
        OCR1B = (uint16_t)new_value;
    }
}

ISR(TIMER1_OVF_vect) {
    overflows++;
}

ISR(TIMER1_CAPT_vect) {
    clocks = ICR1;

    period = (((uint32_t)overflows - (uint32_t)prev_overflows) << 16) + ((uint32_t)clocks - (uint32_t)prev_clocks);

    prev_overflows = overflows;
    prev_clocks = clocks;

    if (period != 10000000) {
        modify_pwm((int32_t)period - 10000000);
    }
}

int main()
{
    init();
    for (;;) {
        _delay_ms(1000);
        uart_write_string("period: ");
        uart_write_uint32(period);
        uart_write_string("\r\n");
    }
    return 0;
}
